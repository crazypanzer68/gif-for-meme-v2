import React, { Component } from 'react'
import { List } from 'antd'
import GifItem from './Item'

class ListGif extends Component {
  render() {
    return (
      <div style={{ minHeight: '300px' , minWidth:'100%'}}>
        <List
          grid={{ gutter: 16, column: 4 }}
          dataSource={this.props.items}
          renderItem={item => (
            <List.Item>
              <GifItem item={item} />
            </List.Item>
          )}
        />
      </div>
    )
  }
}

export default ListGif;