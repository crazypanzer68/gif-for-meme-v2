import React from 'react'
import { Card } from 'antd'

const { Meta } = Card
function GifItem(props) {
  return (
    <Card
      style={{
        height: '240',
        textAlign: 'center'
      }}
      bordered={true}
      onClick={() => {
        props.onItemClick(props.item)
      }}
      hoverable='false'
      cover={<img src={props.item.images.original.url}
        style={{
          height: '240px',
        }}
        alt='' />}>
      <Meta
        title={props.item.title + " "}
      />
    </Card>
  )
}

export default GifItem;
