import React, { Component } from 'react'
import { Card, message, Icon, Form, Button, Input, Layout } from 'antd'
import { auth, provider } from '../firebase'

const Background = `https://media.giphy.com/media/5VKbvrjxpVJCM/giphy.gif`
class Login extends Component {
  state = {
    isLoading: false,
    email: '',
    password: '',
    isShowModal: false,
    isLogin: false,
    imageUrl: ''
  }
  componentDidMount() {
    const jsonStr = localStorage.getItem('user-data')
    const isLoggedIn = jsonStr && JSON.parse(jsonStr).isLoggedIn
    if (isLoggedIn) {
      this.navigateToMainPage()
    }
  }

  navigateToMainPage = () => {
    const { history } = this.props
    history.push('/home')
  }

  saveInformationUser = email => {
    localStorage.setItem(
      'user-data',
      JSON.stringify({
        email: email,
        isLoggedIn: true,
        imageUrl: this.state.imageUrl
      })
    )
    this.setState({ isLoading: false })
    this.navigateToMainPage();
  }

  facebookLogin = () => {
    console.log(this)
    auth.signInWithPopup(provider).then(({ user }) => {
      console.log(this)
      this.setState({ imageUrl: `${user.photoURL}?height=500` })
      this.saveInformationUser(user.email)
    })
  }

  facebookLogout = () => {
    auth.signOut().then(() => {
      this.setState({ user: null })
      message.success('Log out success')
    })
  }

  onEmailChange = event => {
    const email = event.target.value;
    this.setState({ email });
  };

  onPasswordChange = event => {
    const password = event.target.value;
    this.setState({ password });
  };

  validateEmail(email) {
    var re = /^(([^<>()\]\\.,;:\s@"]+(\.[^<>()\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(String(email).toLowerCase());
  }

  validatePassword(password) {
    var re = /^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])[0-9a-zA-Z]{8,}$/;
    return re.test(String(password));
  }

  onSubmitFormLogin = e => {
    e.preventDefault();
    const isValid = this.validateEmail(this.state.email);
    const isValidePassword = this.validatePassword(this.state.password);
    if (isValid && isValidePassword) {
      localStorage.setItem(
        'user-data',
        JSON.stringify({
          isLoggedIn: true,
          email: this.state.email
        })
      );
      this.navigateToMainPage();
    } else {
      message.error('Email or Password invalid!', 1);
    }
  };


  render() {
    return (
      <div type="flex" style={{
        padding: '60px',
        position: 'absolute',
        width: '100%',
        height: '100%',
        minHeight: '100%',
        margin: '0 auto',
        backgroundImage: `url(${Background})`,
        backgroundSize: 'auto',
        backgroundPosition: 'center'
      }}  >
        <Layout
          hoverable
          style={{
            padding: '100px',
            background: 'transparent',
            backgroundColor: 'rgba(0,0,0,0.6)',
            width: '100%',
            height: '100%'
          }}>
          <Form style={{ margin: '0 auto' }} onSubmit={this.onSubmitFormLogin}>
            <Card
              hoverable
              bordered
              style={{ width: 'auto', height: '400px', backgroundColor: '#010000' }}>
              <img style={{ width: 'fixed', height: 75 }}
                src={'https://pmcvariety.files.wordpress.com/2016/10/giphy-logo-e1477932075273.png?w=867&h=490&crop=1'}
                alt='' />
              <h2 style={{ color: '#FFFFFF' }}>LOGIN</h2>
              <Form.Item>
                <Input
                  prefix={<Icon type="user" />}
                  placeholder="Email"
                  onChange={this.onEmailChange}
                />
              </Form.Item>
              <Form.Item>
                <Input
                  prefix={<Icon type="lock" />}
                  type="password"
                  placeholder="Password"
                  onChange={this.onPasswordChange}
                />
              </Form.Item>
              <Form.Item>
                <Button type="primary"
                  htmlType="submit"
                  block
                  onClick={this.onSubmitFormLogin}>
                  Log in
          </Button>
                <Button type="primary"
                  htmlType="submit"
                  block
                  icon="facebook"
                  onClick={this.facebookLogin}>
                  Login With Facebook
          </Button>
                <p style={{ textAlign: 'center', background: '#010000', color: '#FFFFFF' }}>GIF FOR MEME @ FRONT</p>
              </Form.Item>
            </Card>
          </Form>
        </Layout>
      </div>
    )
  }
}

export default Login;