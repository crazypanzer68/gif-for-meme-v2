import { Layout, Menu, Modal, Button, message, Avatar, Icon } from 'antd'
import React, { Component } from 'react'
import RouteMenu from './RouteMenu'
import { CopyToClipboard } from 'react-copy-to-clipboard'

const { Header, Footer, Content } = Layout
const menus = ['home', 'favorite', 'profile']
const apiKey = '8DKRjGuu4cW8GQdhQeQbUxAHh6nbm11S'
const apiTrending = `https://api.giphy.com/v1/gifs/trending?api_key=${apiKey}`
const apiSearch = `https://api.giphy.com/v1/gifs/search?api_key=${apiKey}&q=`
const Limit = `&limit=400&offset=0&rating=R&lang=en`
const Background = `https://media.giphy.com/media/5VKbvrjxpVJCM/giphy.gif`

class MainPage extends Component {
  state = {
    items: [],
    pathname: menus[0],
    email: '',
    itemGif: [],
    favItems: [],
    isShowDailog: false,
    currentSelected: null,
    searchKey: '',
    imageUrl: ''
  }

  componentDidMount() {
    const jsonStr = localStorage.getItem('user-data')
    const email = jsonStr && JSON.parse(jsonStr).email
    const { pathname } = this.props.location
    var pathName = menus[0]
    var imageUrl = jsonStr && JSON.parse(jsonStr).imageUrl
    if (!imageUrl) {
      imageUrl = 'https://icons-for-free.com/free-icons/png/512/1902268.png'
    }

    const jsonFavStr = localStorage.getItem(`list-fav-${email}`)
    if (jsonFavStr) {
      const items = jsonFavStr && JSON.parse(jsonFavStr)
      this.setState({ favItems: items })
    }

    if (pathname !== '/') {
      pathName = pathname.replace('/', '')
      if (!menus.includes(pathName)) pathName = menus[0]
    }

    this.setState({ pathName, email, imageUrl })

    fetch(apiTrending + Limit)
      .then(response => response.json())
      .then(items => this.setState({ items: items.data }))
      .catch(err => console.log(err))
  }

  onClickFavoriteItem = () => {
    const items = this.state.favItems
    const itemFav = this.state.currentSelected
    const index = items.findIndex(item => {
      return item.slug === itemFav.slug
    })
    if (index !== -1) {
      items.splice(index, 1)
      localStorage.setItem(
        `list-fav-${this.state.email}`,
        JSON.stringify(items)
      )
      message.success('unfavorite this item successfully', 1, () => {
        this.setState({ favItems: items })
        this.onModalClickCancel()
        this.reload()
      })
    } else {
      items.push(itemFav)
      localStorage.setItem(
        `list-fav-${this.state.email}`,
        JSON.stringify(items)
      )
      message.success('Saved your favorite this item', 1, () => {
        this.setState({ favItems: items })
        this.onModalClickCancel()
      })
    }
  }

  onSearchChange = (Input) => {
    console.log('Input:', Input)
    if (Input === '') {
      fetch(apiTrending + Limit)
        .then(response => response.json())
        .then(items => this.setState({ items: items.data }))
        .catch(err => console.log(err))
      this.setState({ searchKey: '' })
      this.setState({ page: 0 })

    } else {
      fetch(apiSearch + Input + Limit)
        .then(response => response.json())
        .then(items => this.setState({ items: items.data }))
        .catch(err => console.log(err))
      this.setState({ searchKey: Input })
      this.setState({ page: 0 })

    }
  }

  checkItemFavorited() {
    const items = this.state.favItems
    const itemGif = this.state.currentSelected
    const result = items.find(item => {
      return item.slug === itemGif.slug
    })
    console.log('result', result)

    if (result) {
      return 'primary'
    } else {
      return ''
    }
  }

  onMenuClick = e => {
    var path = '/'
    path = `/${e.key}`
    this.props.history.replace(path)
  }

  onItemClick = item => {
    this.setState({ currentSelected: item })
    this.setState({ isShowDailog: true })
  }

  onModalClickCancel = () => {
    this.setState({ isShowDailog: false })
    this.setState({ currentSelected: null })
  }

  onClickCopy = () => {
    message.success('Copy URL to Clipbord successfully', 1)
  }

  reload() {
    window.location.reload()
  }

  render() {
    const itemGif = this.state.currentSelected
    return (
      <div>
        <Layout>
          <Header
            style={{
              padding: '0px',
              position: 'fixed',
              zIndex: 1,
              width: '100%'
            }}
          >
            <Menu
              theme="dark"
              mode="horizontal"
              style={{ lineHeight: '64px', float: 'right' }}
              defaultSelectedKeys={[this.state.pathname]}
              onClick={e => {
                this.onMenuClick(e)
              }}>
              <Menu.Item key={menus[0]}>Home&nbsp;<Icon type="home" /></Menu.Item>
              <Menu.Item key={menus[1]}>Favorite&nbsp;<Icon type="heart" /></Menu.Item>
              <Menu.Item key={menus[2]}>{[this.state.email]}
                &nbsp;&nbsp;&nbsp;
              <Avatar size="large" src={this.state.imageUrl} />
              </Menu.Item>
            </Menu>
          </Header>
          <Content
            style={{
              padding: '16px',
              marginTop: 64,
              minHeight: '600px',
              minWidth: '100%',
              justifyContent: 'center',
              alignItems: 'center',
              display: 'flex',
              backgroundImage: `url(${Background})`,
              backgroundSize: 'auto',
              backgroundPosition: 'center'
            }}>
            <RouteMenu
              items={this.state.items}
              onItemClick={this.onItemClick}
              onSearchChange={this.onSearchChange}
              onSelectPage={this.onSelectPage}
            />
          </Content>
          <Footer style={{ textAlign: 'center' }}>
            GIF FOR MEME @ FRONT
          </Footer>
        </Layout>
        {itemGif != null ? (
          <div>
            <Modal
              width="50%"
              style={{ maxHeight: '70%' }}
              title={itemGif.title}
              visible={this.state.isShowDailog}
              onCancel={this.onModalClickCancel}
              footer={[
                <Button
                  key="fav"
                  type={this.checkItemFavorited()}
                  icon="heart"
                  size="large"
                  shape="round"
                  onClick={this.onClickFavoriteItem}
                >Favorite</Button>,
                <CopyToClipboard text={itemGif.images.original.url}>
                  <Button
                    key="clipboard"
                    type="primary"
                    icon="link"
                    size="large"
                    shape="round"
                    onClick={this.onClickCopy}
                  >Copy to clipboard</Button>
                </CopyToClipboard>
              ]}
            >
              <div
                style={{
                  display: 'flex',
                  justifyContent: 'center',
                  alignItems: 'center',
                  marginBottom: '16px'
                }}>
                <img
                  src={itemGif.images.original.url}
                  style={{ height: 'auto', width: 'auto' }}
                  alt=''
                />
              </div>
            </Modal>
          </div>
        ) : (
            <div />
          )}
      </div>
    )
  }
}

export default MainPage
