import React, { Component } from 'react'
import { List, Layout, Empty } from 'antd'
import ItemGif from '../Components/List/Item'

class FavoritemPage extends Component {
  state = {
    items: []
  }

  componentDidMount() {
    const jsonStr = localStorage.getItem('user-data')
    const email = jsonStr && JSON.parse(jsonStr).email
    const jsonFavStr = localStorage.getItem(`list-fav-${email}`)
    if (jsonFavStr) {
      const items = jsonFavStr && JSON.parse(jsonFavStr)
      this.setState({ items: items })
    }
  }

  render() {
    // console.log('item :',this.state.items);

    return (
      // <div>Fav</div>
      <div
        style={{
          flex:'3',
          padding: '16px',
          minHeight: '300px',
          justifyContent: 'center',
          alignItems: 'center',
          display: 'flex',
          backgroundColor: 'transparent',
          opacity: '1'
        }}
      >
        <Layout
          style={{
            minWidth:'100%',
            padding: '16px',
            minHeight: '300px',
            justifyContent: 'center',
            alignItems: 'center',
            display: 'flex',
            backgroundColor: 'transparent',
            opacity: '1'
          }}>
          {this.state.items.length > 0 ? (
            <List
              grid={{ gutter: 20, column: 4 }}
              dataSource={this.state.items}
              renderItem={item => (
                <List.Item>
                  <ItemGif
                    item={item}
                    onItemClick={this.props.onItemClick} />
                </List.Item>
              )}
            />
          ) : (
              <Empty />
            )
          }
        </Layout>
      </div>
    )
  }
}

export default FavoritemPage